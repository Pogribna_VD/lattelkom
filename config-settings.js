/** Data for templates */
var data = require('./data/data.js');
var fs = require('fs');

var pages = [];
var entry = {};

var pagesFolder = './source/pages/';
var htmlFiles = [];

var jsFolder = './source/static/js/';
var jsFiles = [];

var item = '';
var itemWithoutEXT = '';

jsFiles = fs.readdirSync(jsFolder);

for(var i = 0; i < jsFiles.length; ++i ) {
	item = jsFiles[i];

	if( !(item.search(/.js/i) == -1) ) {
		itemWithoutEXT = item.replace('.js', '')
		entry[ itemWithoutEXT ] = './static/js/' + jsFiles[i];
	}
}

htmlFiles = fs.readdirSync(pagesFolder);

htmlFiles.forEach( file => {
	pages.push( {
		filename: file.replace('.hbs', '.html'),
		template: './pages/' + file,
		inject: false,
		chunks: ['index', 'common'],
		data: data
	} )
} );

module.exports = {
	pages,
	entry
} 