### To start this build you need: 
* node.js v5 and high
* install global webpack 2.2.10 and higher `npm i -g webpack@version`
* install package dependences `npm install`
* start dev mode `npm run dev`
* start build mode `npm run build`

### Notes: 
* folder "data" it's for handlebars data for pages templates
* template name must match with entry point (js file) name -> e.g. index.hbs and index.js, about.hbs and about.js

### Use sprite:
* normal icons 'name.png' -> `@include sprite($icon)`
* retina icons must have name 'name@2x.png' -> `@include retina-sprite($icon-group)`

### TODO:
* [webpack deploy](https://www.npmjs.com/package/webpack-ssh-deploy-plugin)
* icon font for svg