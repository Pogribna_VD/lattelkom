'use strict';

const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const IconfontWebpackPlugin = require('iconfont-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');
const path = require('path');

const configSettings = require('./config-settings.js');

const isDevelopment = process.env.NODE_ENV == 'dev' ? true : false;

let plugins = [
	new webpack.DefinePlugin({
		PRODUCTION: JSON.stringify(!isDevelopment)
	}),
	new webpack.NoEmitOnErrorsPlugin(),
	new webpack.optimize.CommonsChunkPlugin({
		name: "common",
		filename: isDevelopment ? "./static/js/common.js" : "./static/js/common[hash:16].js",
		minChunks: 2,
	}),
	new ExtractTextPlugin({
		filename: isDevelopment ? './static/css/styles.css' : './static/css/styles[hash:16].css',
		disable: false, 
		allChunks: true 
	}),
	new SpritesmithPlugin({
        src: {
            cwd: __dirname + '/source/static/icons',
            glob: '*.png'
        },
        target: {
            image: __dirname + '/dist/static/img/sprite/sprite.png',
            css: __dirname + '/source/static/scss/_sprite.scss'
        },
        retina: '@2x',
        apiOptions: {
            cssImageRef: "/static/img/sprite/sprite.png"
        }
    }),
    new CleanWebpackPlugin(['dist', 'build'], {
		verbose: true,
		dry: false
	}),
	new CopyWebpackPlugin([
        { 
        	from: __dirname + '/source/static/img', 
        	to: 'static/img',
			fromType: 'glob'
        },
        { 
        	from: __dirname + '/source/static/misc', 
        	to: '',
			fromType: 'glob'
        }
    ]),
	new BrowserSyncPlugin({
		host: 'localhost',
		server: { baseDir: ['dist'] }
	})
];

configSettings.pages.forEach( elem => {
	plugins.push( new HtmlWebpackPlugin( elem ) );
} );

let rules = [
	{
		test: /\.js$/,
		exclude: ['node_modules','dist','build'],
		use: [{
			loader: "babel-loader",
			options: { 
				presets: ["env"],
				plugins: ["babel-plugin-es6-promise"] 
			}
		}],
	},
	{
		test: /\.(svg|eot|woff|woff2|ttf)$/,
		exclude: ['node_modules','misc','dist','build'],
		use: [{
			loader: "file-loader",
			options: {
				name: "/[path][name].[ext]"
			}
		}],
	},
	{
		test: /\.(scss|css)$/,
		exclude: ['node_modules','dist','build'],
		use: ExtractTextPlugin.extract({
			fallback: "style-loader",
			use: [
				{
					loader: 'css-loader',
					options: {
						sourceMap: isDevelopment
					}
				},
				{
					loader: 'postcss-loader',
					options: {
						plugins: () => [
							autoprefixer({
								browsers: ["last 4 version", "Firefox 15", "ie 10"]
							})
						]
					}
				},
				{
					loader: 'sass-loader',
					options: {
						outputStyle: isDevelopment ? 'expanded' : 'compressed',
						sourceMap: isDevelopment
					}
				}
			]
		}),
	},
	{ 
		test: /\.hbs$/, 
		use: [{
			loader: "handlebars-loader",
			query: {
                partialDirs: [ __dirname + '/source/pages_partials' ]
            }
		}]
	}
];

if(!isDevelopment) {
	plugins.push(
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false,
				drop_console: true,
				unsafe: true
			}		
		})
	);
}

module.exports = {
	context: __dirname + "/source",

	entry: configSettings.entry,

	output: {
		path: isDevelopment ? __dirname + "/dist" : __dirname + "/build",
		filename: isDevelopment ? "./static/js/bundle.js" : "./static/js/bundle[hash:16].js"
	},

	watch: isDevelopment,

	watchOptions: {
		aggregateTimeout: 100
	},
	resolve: {
		alias: {
			plugins: path.resolve(__dirname, './source/static/js/plugins'),
			libraries: path.resolve(__dirname, './source/static/js/libraries'),
			common: path.resolve(__dirname, './source/static/js/app/common'),
			components: path.resolve(__dirname, './source/static/js/app/components'),
			templates: path.resolve(__dirname, './source/pages_partials'),
			app: path.resolve(__dirname, './source/static/js/app')
		}
	},
	devtool: isDevelopment ? 'source-map' : 'inline-source-map',
	
	module: {
		rules: rules
	},

	plugins: plugins,

	// externals: isDevelopment ? {} : {
	// 	jquery: 'jQuery'
	// }
};